import download
import sys
import os.path
from pathlib import Path
import shutil

d = download
folder = '/home/m/Pulpit/downloaded_mp3/'

def mac_millan_link(link):
    result = ''
    for i in range(3):
        stop_index = 1 + i*2
        result += link[0:stop_index] + '/'
    result += link
    return result

def replace_some_sign(text):
    text = text.replace('ä', 'ae').replace('ö', 'oe').replace('ü', 'ue').replace('ß', 'ss')
    return text.lower()

def chinese(file_name):
    file = file_name + '.mp3'
    d.download_mp3('https://dictionary.writtenchinese.com/sounds/' + file, file)


def english(file_name):
     file = file_name + '.mp3'
#    d.download_mp3('https://www.diki.pl/images-common/en/mp3/' + file, file)
     sublink = file_name + '_British_English_pronunciation.mp3'
     d.download_mp3('https://www.macmillandictionary.com/media/british/uk_pron/' + mac_millan_link(sublink), file)

def german(file_name):
    file = replace_some_sign(file_name) + '.mp3'
    d.download_mp3('https://www.diki.pl/images-common/de/mp3/' + file, file)

def remove_folder(dir_path):
    shutil.rmtree(dir_path, ignore_errors=True)


def check_dir(dir_path):
    file = Path(dir_path)
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


def run():
    try:
        if lang == 'en':
            english(name)
        elif lang == 'zh':
            chinese(name)
        elif lang == 'de':
            german(name)
        elif lang == 'clear':
             remove_folder(folder)
        else:
            print("Language wasn't recognized")
    except AttributeError:
        print(AttributeError)

try:
    check_dir(folder)
    lang = sys.argv[1].lower()
    if lang != 'clear':
        if (len(sys.argv) == 3):
            name = sys.argv[2]
        else:
            name = sys.argv[2] + '_' + sys.argv[3]
    run()
except IndexError:
    print('You have to input language indentifier as 1st argument,\nand word as 2nd argument')


