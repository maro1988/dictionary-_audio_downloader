from urllib.request import urlopen
import os.path
from pathlib import Path


def download_mp3(link, file_name):
    folder = '/home/m/Pulpit/downloaded_mp3/'
    open_mp3 = open(folder + file_name, 'wb')
    d1 = urlopen(link)
    d2 = d1.read()
    open_mp3.write(d2)
    open_mp3.close()

    print('File {} was saved in {}'.format(file_name, folder))

